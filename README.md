# Lab3 -- Unit testing

[![pipeline status](https://gitlab.com/sqr-inno/s23/s23-lab3-code-conventions-and-unit-testing-amina/badges/master/pipeline.svg)](https://gitlab.com/sqr-inno/s23/s23-lab3-code-conventions-and-unit-testing-amina/-/commits/master)

Files with unit tests:
`src/test/java/com/hw/db/controllers/ForumControllerTests.java`
`src/test/java/com/hw/db/controllers/ThreadControllerTests.java`

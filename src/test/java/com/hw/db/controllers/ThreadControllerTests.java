package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;

import com.hw.db.models.*;
import com.hw.db.models.Thread;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.MockedStatic;
import org.mockito.Mockito;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;

class ThreadControllerTests {
    private Thread thread1;
    private Thread thread2;
    private ThreadController threadController;
    private Post post;
    private User user;

    String author1 = "author1";
    String author2 = "author2";

    String forum1 = "forum1";
    String forum2 = "forum2";

    String message1 = "message1";
    String message2 = "message2";

    String email = "email@mail.ru";
    String about = "about";

    String slug1 = "slug1";
    String slug2 = "slug2";

    String title1 = "title1";
    String title2 = "title2";

    @BeforeEach
    void init() {
        threadController = new ThreadController();
        Timestamp time = new Timestamp(0);

        thread1 = new Thread(author1, time, forum1, message1, slug1, title1, 0);
        thread1.setId(1);

        thread2 = new Thread(author2, time, forum2, message2, slug2, title2, 0);
        thread2.setId(2);

        user = new User(author1, email, author1, about);
        post = new Post(author1, time, forum1, message1, 0, 0, false);
    }

    @Test
    @DisplayName("Test for CheckIdOrSlug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread1);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug1)).thenReturn(thread1);

            assertEquals(threadController.CheckIdOrSlug("1"), thread1);
            assertNull(threadController.CheckIdOrSlug("42"));

            assertEquals(threadController.CheckIdOrSlug(slug1), thread1);
            assertNull(threadController.CheckIdOrSlug("lalala"));
        }
    }

    @Test
    @DisplayName("Test for createPost")
    void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread1);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug1)).thenReturn(thread1);
                userMock.when(() -> UserDAO.Info(author1)).thenReturn(user);
                ResponseEntity resultCreatePost = threadController.createPost(slug1, List.of(post));
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(List.of(post)), resultCreatePost);
            }
        }
    }

    @Test
    @DisplayName("Test for Posts")
    void testPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(author1)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug1)).thenReturn(thread1);
                threadMock.when(() -> ThreadDAO.getPosts(1, 1, 0, "asc", true)).thenReturn(List.of(post));
                ResponseEntity resultPosts = threadController.Posts(slug1, 1, 0, "asc", true);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(List.of(post)), resultPosts);
            }
        }
    }

    @Test
    @DisplayName("Test for change")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread1);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug1)).thenReturn(thread1);

            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
            
            ResponseEntity resultChange = threadController.change(slug2, thread2);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread2), resultChange);
        }
    }

    @Test
    @DisplayName("Test for info")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread1);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug1)).thenReturn(thread1);
            ResponseEntity resultInfo = threadController.info(slug1);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread1), resultInfo);
        }
    }

    @Test
    @DisplayName("createVote method test")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(author1)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread1);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug1)).thenReturn(thread1);
                ResponseEntity resultVote = threadController.createVote(slug1, new Vote(author1, 7));
                assertEquals(HttpStatus.OK, resultVote.getStatusCode());
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread1), resultVote);
            }
        }
    }


}
